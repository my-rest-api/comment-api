package com.comment.model;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by adak on 1/5/2020.
 */
public class Comment {
    String id;
    String ApplicationId;
    String DomainId;
    String SubDomainId;
    String Title;
    String Body;
    String Name;
    String Mobile;
    String Email;
    String UserId;
    String CreatedOn;
    String UpdatedOn;
    boolean IsActive;
    boolean IsDeleted;
    String ParetnId;

    public Comment(String id, String applicatioId, String domainId, String subDomainId, String title, String body, String name, String mobile, String email, String userId, String createdOn, String updatedOn, boolean isActive, boolean isDeleted, String paretnId) {
        this.id = id;
        ApplicationId = applicatioId;
        DomainId = domainId;
        SubDomainId = subDomainId;
        Title = title;
        Body = body;
        Name = name;
        Mobile = mobile;
        Email = email;
        UserId = userId;
        CreatedOn = createdOn;
        UpdatedOn = updatedOn;
        IsActive = isActive;
        IsDeleted = isDeleted;
        ParetnId = paretnId;
    }

    public Comment(String  id,String title, String body, String name, String mobile, String email) {
        this.id = id;
        Title = title;
        Body = body;

    }

    public Comment(){
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApplicatioId() {
        return ApplicationId;
    }

    public void setApplicatioId(String applicatioId) {
        ApplicationId = applicatioId;
    }

    public String getDomainId() {
        return DomainId;
    }

    public void setDomainId(String domainId) {
        DomainId = domainId;
    }

    public String getSubDomainId() {
        return SubDomainId;
    }

    public void setSubDomainId(String subDomainId) {
        SubDomainId = subDomainId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getBody() {
        return Body;
    }

    public void setBody(String body) {
        Body = body;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getUpdatedOn() {
        return UpdatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        UpdatedOn = updatedOn;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    public String getParetnId() {
        return ParetnId;
    }

    public void setParetnId(String paretnId) {
        ParetnId = paretnId;
    }
}
