package com.comment.model;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

//@JsonIgnoreProperties(ignoreUnknown = true)
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class StudentStatus {
 //   @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    int stdstatus;
    String stdstatus_name;



    public StudentStatus(int stdstatus, String stdstatus_name) {

        this.stdstatus = stdstatus;
        this.stdstatus_name = stdstatus_name;
    }

  /*  public String getStdno() {
        return stdno;
    }

    public void setStdno(String stdno) {
        this.stdno = stdno;
    }
*/
    public int getStdstatus() {
        return stdstatus;
    }

    public void setStdstatus(int stdstatus) {
        this.stdstatus = stdstatus;
    }

    public String getStdstatus_name() {
        return stdstatus_name;
    }

    public void setStdstatus_name(String stdstatus_name) {
        this.stdstatus_name = stdstatus_name;
    }
}
