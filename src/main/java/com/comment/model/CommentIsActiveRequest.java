package com.comment.model;

/**
 * Created by adak on 1/31/2020.
 */
public class CommentIsActiveRequest {
    int DomainId;
    int SubDomainId;
    int ApplicationId;
    int Count;
    int Offset;
    Boolean IsActive;

    public CommentIsActiveRequest(int domainId, int subDomainId, int applicationId, int count, int offset, Boolean isActive) {
        DomainId = domainId;
        SubDomainId = subDomainId;
        ApplicationId = applicationId;
        Count = count;
        Offset = offset;
        IsActive = isActive;
    }
    public  CommentIsActiveRequest(){}

    public int getDomainId() {
        return DomainId;
    }

    public void setDomainId(int domainId) {
        DomainId = domainId;
    }

    public int getSubDomainId() {
        return SubDomainId;
    }

    public void setSubDomainId(int subDomainId) {
        SubDomainId = subDomainId;
    }

    public int getApplicationId() {
        return ApplicationId;
    }

    public void setApplicationId(int applicationId) {
        ApplicationId = applicationId;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    public int getOffset() {
        return Offset;
    }

    public void setOffset(int offset) {
        Offset = offset;
    }

    public Boolean getActive() {
        return IsActive;
    }

    public void setActive(Boolean active) {
        IsActive = active;
    }
}
