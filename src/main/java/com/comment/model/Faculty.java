package com.comment.model;

public class Faculty {
    int facno;
    String facname;
    int grpno;
    String grp_name;

    public Faculty(int facno, String facname, int grpno, String grp_name) {
        this.facno = facno;
        this.facname = facname;
        this.grpno = grpno;
        this.grp_name = grp_name;
    }

    public Faculty(int facno) {
        this.facno = facno;
    }

    public Faculty(int facno, int grpno) {
        this.facno = facno;
        this.grpno = grpno;
    }

    public int getFacno() {
        return facno;
    }

    public void setFacno(int facno) {
        this.facno = facno;
    }

    public String getFacname() {
        return facname;
    }

    public void setFacname(String facname) {
        this.facname = facname;
    }

    public int getGrpno() {
        return grpno;
    }

    public void setGrpno(int grpno) {
        this.grpno = grpno;
    }

    public String getGrp_name() {
        return grp_name;
    }

    public void setGrp_name(String grp_name) {
        this.grp_name = grp_name;
    }

}
