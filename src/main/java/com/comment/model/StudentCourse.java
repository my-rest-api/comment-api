package com.comment.model;

public class StudentCourse {
    String stdno;
    String crsmode;
    String lesno;
    String lesnofrp;

    public StudentCourse(String stdno, String crsmode, String lesno, String lesnofrp) {
        this.stdno = stdno;
        this.crsmode = crsmode;
        this.lesno = lesno;
        this.lesnofrp = lesnofrp;
    }

    public String getStdno() {
        return stdno;
    }

    public void setStdno(String stdno) {
        this.stdno = stdno;
    }

    public String getCrsmode() {
        return crsmode;
    }

    public void setCrsmode(String crsmode) {
        this.crsmode = crsmode;
    }

    public String getLesno() {
        return lesno;
    }

    public void setLesno(String lesno) {
        this.lesno = lesno;
    }

    public String getLesnofrp() {
        return lesnofrp;
    }

    public void setLesnofrp(String lesnofrp) {
        this.lesnofrp = lesnofrp;
    }
}
