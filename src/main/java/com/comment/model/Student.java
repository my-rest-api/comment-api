package com.comment.model;






public class Student {

    String stdno;
    String family;
    String name;
    int entrtrm;
    int stdstatus;
    String stdstatus_name;
    String email;
    String mobileno;
    String  ssno;
    int lvlno;
    String lvl_name;
    int facno;
    String fac_name;
    int grpno;
    String grp_name;
    int depno;
    String dep_name;
    int lsysno;
    String lsys_name;

    public Student(String stdno, String family, String name, int entrtrm, int stdstatus, String stdstatus_name, String email, String mobileno, String ssno, int lvlno, String lvl_name, int facno, String fac_name, int grpno, String grp_name, int depno, String dep_name, int lsysno, String lsys_name) {
        this.stdno = stdno;
        this.family = family;
        this.name = name;
        this.entrtrm = entrtrm;
        this.stdstatus = stdstatus;
        this.stdstatus_name = stdstatus_name;
        this.email = email;
        this.mobileno = mobileno;
        this.ssno = ssno;
        this.lvlno = lvlno;
        this.lvl_name = lvl_name;
        this.facno = facno;
        this.fac_name = fac_name;
        this.grpno = grpno;
        this.grp_name = grp_name;
        this.depno = depno;
        this.dep_name = dep_name;
        this.lsysno = lsysno;
        this.lsys_name = lsys_name;
    }

    public Student(int stdstatus, String stdstatus_name) {
        this.stdstatus = stdstatus;
        this.stdstatus_name = stdstatus_name;
    }

    public String getStdno() {
        return stdno;
    }

    public void setStdno(String stdno) {
        this.stdno = stdno;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEntrtrm() {
        return entrtrm;
    }

    public void setEntrtrm(int entrtrm) {
        this.entrtrm = entrtrm;
    }

    public int getStdstatus() {
        return stdstatus;
    }

    public void setStdstatus(int stdstatus) {
        this.stdstatus = stdstatus;
    }

    public String getStdstatus_name() {
        return stdstatus_name;
    }

    public void setStdstatus_name(String stdstatus_name) {
        this.stdstatus_name = stdstatus_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getSsno() {
        return ssno;
    }

    public void setSsno(String ssno) {
        this.ssno = ssno;
    }

    public int getLvlno() {
        return lvlno;
    }

    public void setLvlno(int lvlno) {
        this.lvlno = lvlno;
    }

    public String getLvl_name() {
        return lvl_name;
    }

    public void setLvl_name(String lvl_name) {
        this.lvl_name = lvl_name;
    }

    public int getFacno() {
        return facno;
    }

    public void setFacno(int facno) {
        this.facno = facno;
    }

    public String getFac_name() {
        return fac_name;
    }

    public void setFac_name(String fac_name) {
        this.fac_name = fac_name;
    }

    public int getGrpno() {
        return grpno;
    }

    public void setGrpno(int grpno) {
        this.grpno = grpno;
    }

    public String getGrp_name() {
        return grp_name;
    }

    public void setGrp_name(String grp_name) {
        this.grp_name = grp_name;
    }

    public int getDepno() {
        return depno;
    }

    public void setDepno(int depno) {
        this.depno = depno;
    }

    public String getDep_name() {
        return dep_name;
    }

    public void setDep_name(String dep_name) {
        this.dep_name = dep_name;
    }

    public int getLsysno() {
        return lsysno;
    }

    public void setLsysno(int lsysno) {
        this.lsysno = lsysno;
    }

    public String getLsys_name() {
        return lsys_name;
    }

    public void setLsys_name(String lsys_name) {
        this.lsys_name = lsys_name;
    }
}
