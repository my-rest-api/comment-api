package com.comment.model;

/**
 * Created by adak on 1/31/2020.
 */
public class CommentRequest {
    String DomainId;
    String SubDomainId;
    String ApplicationId;
    int Count;
    int Offset;


    public CommentRequest(String domainId, String subDomainId, String applicationId , int count) {
        DomainId = domainId;
        SubDomainId = subDomainId;
        ApplicationId = applicationId;
        int Count;
        int Offset;
    }
    public CommentRequest(){}

    public String getDomainId() {
        return DomainId;
    }

    public void setDomainId(String domainId) {
        DomainId = domainId;
    }

    public String getSubDomainId() {
        return SubDomainId;
    }

    public void setSubDomainId(String subDomainId) {
        SubDomainId = subDomainId;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    public String getApplicationId() {
        return ApplicationId;
    }

    public void setApplicationId(String applicationId) {
        ApplicationId = applicationId;
    }

    public int getOffset() {
        return Offset;
    }

    public void setOffset(int offset) {
        Offset = offset;
    }
}
