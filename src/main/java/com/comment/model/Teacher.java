package com.comment.model;

public class Teacher {
    int tchno;
    String tch_family;
    String tch_name;
    String martabeh_dcs;
    String martabeh;
    String major;
    String major_name;
    String email;
    String mobileno;
    String tch_photo;
    String facno;
    String grpno;
    String ssno;
    String grp_name;
    String fac_name;

    public Teacher(int tchno, String tch_family, String tch_name, String martabeh_dcs, String martabeh, String major, String major_name, String email, String mobileno, String tch_photo, String facno, String grpno, String ssno, String grp_name, String fac_name) {
        this.tchno = tchno;
        this.tch_family = tch_family;
        this.tch_name = tch_name;
        this.martabeh_dcs = martabeh_dcs;
        this.martabeh = martabeh;
        this.major = major;
        this.major_name = major_name;
        this.email = email;
        this.mobileno = mobileno;
        this.tch_photo = tch_photo;
        this.facno = facno;
        this.grpno = grpno;
        this.ssno = ssno;
        this.grp_name = grp_name;
        this.fac_name = fac_name;
    }

    public int getTchno() {
        return tchno;
    }

    public void setTchno(int tchno) {
        this.tchno = tchno;
    }

    public String getTch_family() {
        return tch_family;
    }

    public void setTch_family(String tch_family) {
        this.tch_family = tch_family;
    }

    public String getTch_name() {
        return tch_name;
    }

    public void setTch_name(String tch_name) {
        this.tch_name = tch_name;
    }

    public String getMartabeh_dcs() {
        return martabeh_dcs;
    }

    public void setMartabeh_dcs(String martabeh_dcs) {
        this.martabeh_dcs = martabeh_dcs;
    }

    public String getMartabeh() {
        return martabeh;
    }

    public void setMartabeh(String martabeh) {
        this.martabeh = martabeh;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getMajor_name() {
        return major_name;
    }

    public void setMajor_name(String major_name) {
        this.major_name = major_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getTch_photo() {
        return tch_photo;
    }

    public void setTch_photo(String tch_photo) {
        this.tch_photo = tch_photo;
    }

    public String getFacno() {
        return facno;
    }

    public void setFacno(String facno) {
        this.facno = facno;
    }

    public String getGrpno() {
        return grpno;
    }

    public void setGrpno(String grpno) {
        this.grpno = grpno;
    }

    public String getSsno() {
        return ssno;
    }

    public void setSsno(String ssno) {
        this.ssno = ssno;
    }

    public String getGrp_name() {
        return grp_name;
    }

    public void setGrp_name(String grp_name) {
        this.grp_name = grp_name;
    }

    public String getFac_name() {
        return fac_name;
    }

    public void setFac_name(String fac_name) {
        this.fac_name = fac_name;
    }
}
