package com.comment.model;

public class TeacherCourse {
 int tchno;
 String fac_name;
 int facno;
 String crs_name;
 String lvl_name;
 int lvlno;
 int tmno;
 int lesno;
 int lesnoid;

    public TeacherCourse(int tchno, String fac_name, int facno, String crs_name, String lvl_name, int lvlno, int tmno, int lesno, int lesnoid) {
        this.tchno = tchno;
        this.fac_name = fac_name;
        this.facno = facno;
        this.crs_name = crs_name;
        this.lvl_name = lvl_name;
        this.lvlno = lvlno;
        this.tmno = tmno;
        this.lesno = lesno;
        this.lesnoid = lesnoid;
    }

    public int getTchno() {
        return tchno;
    }

    public void setTchno(int tchno) {
        this.tchno = tchno;
    }

    public String getFac_name() {
        return fac_name;
    }

    public void setFac_name(String fac_name) {
        this.fac_name = fac_name;
    }

    public int getFacno() {
        return facno;
    }

    public void setFacno(int facno) {
        this.facno = facno;
    }

    public String getCrs_name() {
        return crs_name;
    }

    public void setCrs_name(String crs_name) {
        this.crs_name = crs_name;
    }

    public String getLvl_name() {
        return lvl_name;
    }

    public void setLvl_name(String lvl_name) {
        this.lvl_name = lvl_name;
    }

    public int getLvlno() {
        return lvlno;
    }

    public void setLvlno(int lvlno) {
        this.lvlno = lvlno;
    }

    public int getTmno() {
        return tmno;
    }

    public void setTmno(int tmno) {
        this.tmno = tmno;
    }

    public int getLesno() {
        return lesno;
    }

    public void setLesno(int lesno) {
        this.lesno = lesno;
    }

    public int getLesnoid() {
        return lesnoid;
    }

    public void setLesnoid(int lesnoid) {
        this.lesnoid = lesnoid;
    }
}
