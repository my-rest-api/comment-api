package com.comment.doa;

import com.comment.exceptions.GenericException;
import com.comment.model.*;
//import com.sun.istack.internal.Nullable;
import javax.annotation.Nullable;


import javax.jws.soap.SOAPBinding;
import java.util.*;
import java.sql.*;
import java.util.Date;


public class siteService {
    private static Connection sqlConnection= null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;

    //   private JDBCConnection jdbcConnection = null commented now
    private static JDBCConnection jdbcConnection = null;
    private static siteService siteServiceInstance;

       private siteService(){
          try {
              if (sqlConnection == null) {
                   sqlConnection = jdbcConnection.getSqlConnnection();
                   System.out.println("it is creating a new connection");

               }
           } catch (SQLException e) {
               e.printStackTrace();
               throw new GenericException("database error siteservise");
           }
       }


    public static siteService getInstance(){
        if (siteServiceInstance == null){ //if there is no instance available... create new one
            System.out.println("no instance of siteService is available, so create new one");
            siteServiceInstance = new siteService();
        }
        System.out.println("an instance of siteService is available");
        return siteServiceInstance;
    }

    private Connection checkSqlCon(Connection sqlCon) {
        try {
            if (sqlCon == null || sqlCon.isClosed()) {
                try {
                    System.out.println("sql is not connected!");
                    sqlConnection = jdbcConnection.getSqlConnnection();
                    System.out.println("sql is connected now");


                } catch (SQLException e) {
                    e.printStackTrace();
                }
                System.out.println();

            } else {
                sqlConnection = sqlCon;
                System.out.println("sql connection is valid");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sqlConnection;
    }

///////////////////////getAllComments/////////////////////////////


    public String getSqlCommand (int count, int offset, @Nullable Boolean isActive){
        String isActiveCondition = "";
        if (isActive != null){
         if(isActive)
         {isActiveCondition = " and isactive= 1 " ;}
         else
         {isActiveCondition=" and isactive = 0 ";}
        }
        if (count == 0){
            String sql = "select * from Comment WHERE IsDeleted = 0 and DomainId = ? and  SubDomainId = ? and applicationId = ?"+isActiveCondition;
            return sql;
        }else if(offset == 0) {
            String sql = "select TOP " + count + " * from Comment  WHERE DomainId = ? and  SubDomainId = ? and applicationId = ?"+isActiveCondition+" order by id ";
            return sql;
        }else {
            String sql = "select * from Comment  WHERE DomainId = ? and  SubDomainId = ? and applicationId = ? "+isActiveCondition+" order by id  offset " + offset + " rows fetch next "+count+" rows only ";

            return sql;
        }

    }
    public ArrayList<Comment> getAllCommentsService(CommentRequest commentRequest)  {
        int count = commentRequest.getCount();
        int offset = commentRequest.getOffset();
        String sql = getSqlCommand(count,offset, null);
        String domainId = commentRequest.getDomainId();
        String subDomainId = commentRequest.getSubDomainId();
        String applicationId = commentRequest.getApplicationId();
        String UpdatedOn = "";
    ArrayList<Comment> commentList = new ArrayList<Comment>();
    System.out.println(sql);
    try{
        PreparedStatement pstmt = checkSqlCon(sqlConnection).prepareStatement(sql);
        pstmt.setString(1, domainId);
        pstmt.setString(2, subDomainId);
        pstmt.setString(3,applicationId);
        System.out.println("sql is: " + sql);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            String Id = rs.getString(1);
            String ApplicationId = rs.getString(2);
            String DomainId = rs.getString(3);
            String SubDomainId = rs.getString(4);
            String Title = rs.getString(5);
            String Body = rs.getString(6);
            String Name= rs.getString(7);
            String Mobile = rs.getString(8);
            String Email = rs.getString(9);
            String UserId = rs.getString(10);
            String CreatedOn = rs.getTimestamp(11).toString();
            if (rs.getTimestamp(12)!=null){
            UpdatedOn = rs.getTimestamp(12).toString();}
            boolean IsActive = rs.getBoolean(13);
            boolean IsDeleted = rs.getBoolean(14);
            String ParentId = rs.getString(15);

            commentList.add(new Comment(Id,ApplicationId, DomainId, SubDomainId, Title, Body, Name, Mobile, Email, UserId, CreatedOn, UpdatedOn, IsActive, IsDeleted, ParentId));        }
    }catch (SQLException e ) {
        e.printStackTrace();
        System.out.println("query execution  has an issue.");
    }finally {
        if (rs != null) {
            try {
                rs.close();
                System.out.println("result set is closed");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("resultset connection is not closed properly");
            }
        }
        if (pstmt != null) {
            try {
                pstmt.close();
                System.out.println("pstmt is closed");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("pstmt is not closed properly");
            }
        }
        if (sqlConnection != null) {
            try {
                sqlConnection.close();
                System.out.println("sql connection is closed");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("sql connection is not closed properly");
            }
        }
    }

    return commentList;
}


/*************getActiveComments*****************/

    public ArrayList<Comment> getActiveCommentsService(CommentIsActiveRequest commentIsActiveRequest)  {
        int count = commentIsActiveRequest.getCount();
        int offset = commentIsActiveRequest.getOffset();
        Boolean isActive = commentIsActiveRequest.getActive();
        String sql = getSqlCommand(count,offset, isActive);
        int domainId = commentIsActiveRequest.getDomainId();
        int subDomainId = commentIsActiveRequest.getSubDomainId();
        int applicationId = commentIsActiveRequest.getApplicationId();

        ArrayList<Comment> commentList = new ArrayList<Comment>();
        System.out.println(sql);
        try{
            PreparedStatement pstmt = checkSqlCon(sqlConnection).prepareStatement(sql);
            pstmt.setInt(1, domainId);
            pstmt.setInt(2, subDomainId);
            pstmt.setInt(3,applicationId);
            System.out.println("sql is: " + sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                String Id = rs.getString(1);
                String ApplicationId = rs.getString(2);
                String DomainId = rs.getString(3);
                String SubDomainId = rs.getString(4);
                String Title = rs.getString(5);
                String Body = rs.getString(6);
                String Name= rs.getString(7);
                String Mobile = rs.getString(8);
                String Email = rs.getString(9);
                String UserId = rs.getString(10);
                String CreatedOn = rs.getTimestamp(11).toString();
                String UpdatedOn = rs.getTimestamp(12).toString();
                boolean IsActive = rs.getBoolean(13);
                boolean IsDeleted = rs.getBoolean(14);
                String ParentId = rs.getString(15);

                commentList.add(new Comment(Id,ApplicationId, DomainId, SubDomainId, Title, Body, Name, Mobile, Email, UserId, CreatedOn, UpdatedOn, IsActive, IsDeleted, ParentId));        }
        }catch (SQLException e ) {
            e.printStackTrace();
            System.out.println("query execution  has an issue.");
        }finally {
            if (rs != null) {
                try {
                    rs.close();
                    System.out.println("result set is closed");
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("resultset connection is not closed properly");
                }
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                    System.out.println("pstmt is closed");
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("pstmt is not closed properly");
                }
            }
            if (sqlConnection != null) {
                try {
                    sqlConnection.close();
                    System.out.println("sql connection is closed");
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("sql connection is not closed properly");
                }
            }
        }

        return commentList;
    }

/*************addComment*****************/
    public Boolean addCommentsService(Comment cmnt)  {

    String sql = "insert into Comment(ApplicationId,DomainId,SubDomainId,Title,Body,Name,Mobile,Email,UserId,CreatedOn,UpdatedOn,IsActive,IsDeleted, ParentId) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    System.out.println(sql);
    try{
        PreparedStatement pstmt = checkSqlCon(sqlConnection).prepareStatement(sql);

        Timestamp createdOn = new Timestamp(new Date().getTime());
        System.out.println(createdOn);
        Timestamp updatedOn = null;
        Boolean isActive = false;
        Boolean isDeleted = false;

        pstmt.setString(1, cmnt.getApplicatioId());
        pstmt.setString(2, cmnt.getDomainId());
        pstmt.setString(3, cmnt.getSubDomainId());
        pstmt.setString(4, cmnt.getTitle());
        pstmt.setString(5, cmnt.getBody());
        pstmt.setString(6, cmnt.getName());
        pstmt.setString(7, cmnt.getMobile());
        pstmt.setString(8, cmnt.getEmail());
        pstmt.setString(9, cmnt.getUserId());
        pstmt.setTimestamp(10, createdOn);
        pstmt.setTimestamp(11, updatedOn);
        pstmt.setBoolean(12,isActive);
        pstmt.setBoolean(13,isDeleted);
        pstmt.setString(14,cmnt.getParetnId());

        System.out.println("sql is: " + sql);
        //  System.out.println("inside of faculityAPI1, after checking connection");
        pstmt.execute();
       // ResultSet rs=pstmt.getResultSet();
        pstmt.close();

    }catch (SQLException e ) {
        e.printStackTrace();
        System.out.println("query execution  has an issue.");
    }finally {
        if (rs != null) {
            try {
                rs.close();
                System.out.println("result set is closed");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("resultset connection is not closed properly");
            }
        }
        if (pstmt != null) {
            try {
                pstmt.close();
                System.out.println("pstmt is closed");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("pstmt is not closed properly");
            }
        }

        if (sqlConnection != null) {
            try {
                sqlConnection.close();
                System.out.println("sql connection is closed");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("sql connection is not closed properly");
            }
        }
    }
    return true;
}
public Boolean deleteCommentsService(int id){
    String sql = "update Comment set IsDeleted = 1 ,updatedOn = ? WHERE id = ?";
    Timestamp updatedOn = new Timestamp(new Date().getTime());
    try{
    PreparedStatement pstmt = checkSqlCon(sqlConnection).prepareStatement(sql);
        pstmt.setTimestamp(1,updatedOn);
    pstmt.setInt(2, id);
    System.out.println("sql is: " + sql);
    pstmt.executeUpdate();
    pstmt.close();
}catch (SQLException e ) {
        e.printStackTrace();
        System.out.println("query execution  has an issue.");
    }finally {
        if (sqlConnection != null) {
            try {
                sqlConnection.close();
                System.out.println("sql connection is closed");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("sql connection is not closed properly");
            }
        }
    }
    return true;

}
/////////********activate Comments*********//////////////
    public Boolean activeCommentsService(int id){
        String sql = "update Comment set IsActive = 1 ,updatedOn = ? WHERE id = ?";
        Timestamp updatedOn = new Timestamp(new Date().getTime());
        try{
            PreparedStatement pstmt = checkSqlCon(sqlConnection).prepareStatement(sql);
            pstmt.setTimestamp(1,updatedOn);
            pstmt.setInt(2, id);
            System.out.println("sql is: " + sql);
            pstmt.executeUpdate();
            pstmt.close();
        }catch (SQLException e ) {
            e.printStackTrace();
            System.out.println("query execution  has an issue.");
        }finally {
            if (sqlConnection != null) {
                try {
                    sqlConnection.close();
                    System.out.println("sql connection is closed");
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("sql connection is not closed properly");
                }
            }
        }
        return true;

    }
/////////********activate Comments*********//////////////
    public Boolean updateCommentsService(Comment comment){
        String id = comment.getId();
        String title = comment.getTitle();
        String body = comment.getBody();
        Timestamp updatedOn = new Timestamp(new Date().getTime());
        String sql = "update Comment set Title = ?, Body = ?  ,updatedOn = ? WHERE id = ?";
        try{
            PreparedStatement pstmt = checkSqlCon(sqlConnection).prepareStatement(sql);
            pstmt.setString(1,title);
            pstmt.setString(2,body);
            pstmt.setTimestamp(3,updatedOn);
            pstmt.setString(4, id);
            System.out.println("sql is: " + sql);
            pstmt.executeUpdate();
            pstmt.close();
        }catch (SQLException e ) {
            e.printStackTrace();
            System.out.println("query execution  has an issue.");
        }finally {
            if (sqlConnection != null) {
                try {
                    sqlConnection.close();
                    System.out.println("sql connection is closed");
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("sql connection is not closed properly");
                }
            }
        }


        return true;
    }
}