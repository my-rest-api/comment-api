package com.comment.doa;
import com.comment.exceptions.GenericException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.*;

public class JDBCConnection {
    private static final String DB_DRIVER_CLASS = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private static final String DB_URL = "jdbc:sqlserver://localhost;databaseName=Comment";
    private static final String DB_USERNAME = "sa";
    private static final String DB_PASSWORD = "123";
    private static String driverName = "";
    private static Connection hiveConnection = null;
    private static Connection mysqlConnection = null;
    private static Connection sqlConnection = null;

    public static Connection getHiveConnnection() throws SQLException {
            try {
                Class.forName(driverName);
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                System.exit(1);
            }
            try {
                hiveConnection = DriverManager.getConnection("jdbc:hive2://XXX", "", "");

            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.getLocalizedMessage();
            }
        return hiveConnection;
    }

    public static Connection getSqlConnnection() throws SQLException {
            try {
                // load the Driver Class
                Class.forName(DB_DRIVER_CLASS);
                // create the connection now
               sqlConnection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
                if( sqlConnection==null){
                    throw new GenericException("An error in sql getConnection,Database username or password is wrong");
                }
            } catch (ClassNotFoundException e) {
                System.out.println("class not found execption database driver has some problem");
                e.printStackTrace();

            } catch (SQLException e) {
               // e.printStackTrace();

                System.out.println("sql get connection error, database username or password is not correct");
                throw new GenericException("user pass database error");
            }
        return sqlConnection;
    }



}
