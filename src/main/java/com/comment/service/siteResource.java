package com.comment.service;
import com.comment.model.*;
import com.comment.doa.siteService;
import com.comment.exceptions.DataNotFoundException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@Path("/v1/comment")
public class siteResource {
    siteService ss =  siteService.getInstance();



 /*   @GET
    @Path("/students")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Student> getAPI1InJOSN() throws SQLException {
        ArrayList<Student>  apiDataList = new ArrayList<Student>();
        apiDataList =  ss.faculityAPI1();
        if(apiDataList.size() == 0) {
            throw new DataNotFoundException("no data is found with");
        }
        return  ss.faculityAPI1();
    }

  */
 /**Return All Active and Deactive (but Not Deleted) Comments**/
    @POST
    @Path("/get/allComments")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response<ArrayList<Comment>> getAllComments(CommentRequest commentRequest) throws SQLException {
        ArrayList<Comment>  apiDataList = new ArrayList<Comment>();
        Response<ArrayList<Comment>> response=new Response<>();
        ResponseStatus responseStatus =new ResponseStatus();

        apiDataList =  ss.getAllCommentsService(commentRequest);
        if(apiDataList.size() == 0) {
            //throw new DataNotFoundException("no data is found with:" + commentRequest );
            responseStatus.setCode(ErrorCode.NOTFOUND);
            responseStatus.setMessage("no data is found");
            response.setResponseStatus(responseStatus);
        }
        else {
            responseStatus.setCode(ErrorCode.SUCCESS);
            responseStatus.setMessage("Successfull");
            response.setResponseStatus(responseStatus);
            response.setResult(apiDataList);
        }
        return response;

    }
    @POST
    @Path("/get/activeComments")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response<ArrayList<Comment>> getActiveComments(CommentIsActiveRequest commentIsActiveRequest) throws SQLException {
        Response<ArrayList<Comment>> response=new Response<>();
        ResponseStatus responseStatus =new ResponseStatus();
        ArrayList<Comment>  apiDataList = new ArrayList<Comment>();
        apiDataList =  ss.getActiveCommentsService(commentIsActiveRequest);
        if(apiDataList.size() == 0) {
            //throw new DataNotFoundException("no data is found with:" + commentRequest );
            responseStatus.setCode(ErrorCode.NOTFOUND);
            responseStatus.setMessage("no data is found");
            response.setResponseStatus(responseStatus);
        }
        else {
            responseStatus.setCode(ErrorCode.SUCCESS);
            responseStatus.setMessage("Successfull");
            response.setResponseStatus(responseStatus);
            response.setResult(apiDataList);
        }
        return response;
    }

    @POST
    @Path("/new")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response<Boolean> addComment(Comment comment) {
        Response<Boolean> response=new Response<>();
        ResponseStatus responseStatus =new ResponseStatus();
        Boolean result =   ss.addCommentsService(comment);
        //throw new DataNotFoundException("no data is found with:" + commentRequest );
        if(result != true){
            responseStatus.setCode(ErrorCode.NOTFOUND);
            responseStatus.setMessage("no data is found");
            response.setResponseStatus(responseStatus);
    }
        else {
        responseStatus.setCode(ErrorCode.SUCCESS);
        responseStatus.setMessage("Successfull");
        response.setResponseStatus(responseStatus);
        response.setResult(result);
    }
        return response;

    }

    @GET
    @Path("/delete/id/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<Boolean> deleteComment(@PathParam("id") int id) {
        Response<Boolean> response=new Response<>();
        ResponseStatus responseStatus =new ResponseStatus();
        Boolean result =   ss.deleteCommentsService(id);
        //throw new DataNotFoundException("no data is found with:" + commentRequest );
        if(result != true){
            responseStatus.setCode(ErrorCode.NOTFOUND);
            responseStatus.setMessage("no data is found");
            response.setResponseStatus(responseStatus);
        }
        else {
            responseStatus.setCode(ErrorCode.SUCCESS);
            responseStatus.setMessage("Successfull");
            response.setResponseStatus(responseStatus);
            response.setResult(result);
        }
        return response;

    }
    @GET
    @Path("/active/id/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<Boolean> activeComment(@PathParam("id") int id) {
        Response<Boolean> response=new Response<>();
        ResponseStatus responseStatus =new ResponseStatus();
        Boolean result =   ss.activeCommentsService(id);
        //throw new DataNotFoundException("no data is found with:" + commentRequest );
        if(result != true){
            responseStatus.setCode(ErrorCode.NOTFOUND);
            responseStatus.setMessage("no data is found");
            response.setResponseStatus(responseStatus);
        }
        else {
            responseStatus.setCode(ErrorCode.SUCCESS);
            responseStatus.setMessage("Successfull");
            response.setResponseStatus(responseStatus);
            response.setResult(result);
        }
        return response;

    }
    @POST
    @Path("/update")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response<Boolean> updateComment(Comment comment) {
        Response<Boolean> response=new Response<>();
        ResponseStatus responseStatus =new ResponseStatus();
        Boolean result =   ss.updateCommentsService(comment);
        //throw new DataNotFoundException("no data is found with:" + commentRequest );
        if(result != true){
            responseStatus.setCode(ErrorCode.NOTFOUND);
            responseStatus.setMessage("no data is found");
            response.setResponseStatus(responseStatus);
        }
        else {
            responseStatus.setCode(ErrorCode.SUCCESS);
            responseStatus.setMessage("Successfull");
            response.setResponseStatus(responseStatus);
            response.setResult(result);
        }
        return response;

    }


}

