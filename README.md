We have a problem with creating a new instance of siteService class with each API call.
so this issue results in some problem with resource consumption
so to solve the problem we change the construction definition to be designed as a Singleton.
 to do that we changed the siteService constructor definition to: 
 
        private siteService(){
          try {
              if (sqlConnection == null) {
                   sqlConnection = jdbcConnection.getSqlConnnection();
                   System.out.println("it is creating a new connection");

               }
           } catch (SQLException e) {
               e.printStackTrace();
               throw new GenericException("database error siteservise");
           }
       }

and also we insert a new class which the name is getInstance:

    public static siteService getInstance(){
        if (siteServiceInstance == null){ //if there is no instance available... create new one
            System.out.println("no instance of siteService is available, so create new one");
            siteServiceInstance = new siteService();
        }
        System.out.println("an instance of siteService is available");
        return siteServiceInstance;
    }
	
and now in the siteResource we change:
from:
    siteService ss =  new siteService();
	
to:
    siteService ss =  siteService.getInstance();	
	